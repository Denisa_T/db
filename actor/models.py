from __future__ import unicode_literals

from django.db import models


class Actor(models.Model):
	actor_id = models.SmallIntegerField(primary_key=True)
	first_name = models.CharField(max_length=45)
	last_name = models.CharField(max_length=45)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'actor'


class Address(models.Model):
	address_id = models.SmallIntegerField(primary_key=True)
	address = models.CharField(max_length=50)
	address2 = models.CharField(max_length=50, blank=True, null=True)
	district = models.CharField(max_length=20)
	city = models.ForeignKey('City', models.DO_NOTHING)
	postal_code = models.CharField(max_length=10, blank=True, null=True)
	phone = models.CharField(max_length=20)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'address'


class AuthGroup(models.Model):
	name = models.CharField(unique=True, max_length=80)

	class Meta:
		managed = False
		db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
	group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
	permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

	class Meta:
		managed = False
		db_table = 'auth_group_permissions'
		unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
	name = models.CharField(max_length=255)
	content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
	codename = models.CharField(max_length=100)

	class Meta:
		managed = False
		db_table = 'auth_permission'
		unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
	password = models.CharField(max_length=128)
	last_login = models.DateTimeField(blank=True, null=True)
	is_superuser = models.IntegerField()
	username = models.CharField(unique=True, max_length=30)
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	email = models.CharField(max_length=254)
	is_staff = models.IntegerField()
	is_active = models.IntegerField()
	date_joined = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'auth_user'


class AuthUserGroups(models.Model):
	user = models.ForeignKey(AuthUser, models.DO_NOTHING)
	group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

	class Meta:
		managed = False
		db_table = 'auth_user_groups'
		unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
	user = models.ForeignKey(AuthUser, models.DO_NOTHING)
	permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

	class Meta:
		managed = False
		db_table = 'auth_user_user_permissions'
		unique_together = (('user', 'permission'),)


class Category(models.Model):
	category_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=25)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'category'


class City(models.Model):
	city_id = models.SmallIntegerField(primary_key=True)
	city = models.CharField(max_length=50)
	country = models.ForeignKey('Country', models.DO_NOTHING)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'city'


class Country(models.Model):
	country_id = models.SmallIntegerField(primary_key=True)
	country = models.CharField(max_length=50)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'country'


class Customer(models.Model):
	customer_id = models.SmallIntegerField(primary_key=True)
	#store = models.ForeignKey('Store', models.DO_NOTHING)
	first_name = models.CharField(max_length=45)
	last_name = models.CharField(max_length=45)
	email = models.CharField(max_length=50, blank=True, null=True)
	address = models.ForeignKey(Address, models.DO_NOTHING)
	active = models.IntegerField()
	create_date = models.DateTimeField()
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'customer'


class DjangoAdminLog(models.Model):
	action_time = models.DateTimeField()
	object_id = models.TextField(blank=True, null=True)
	object_repr = models.CharField(max_length=200)
	action_flag = models.SmallIntegerField()
	change_message = models.TextField()
	content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
	user = models.ForeignKey(AuthUser, models.DO_NOTHING)

	class Meta:
		managed = False
		db_table = 'django_admin_log'


class DjangoContentType(models.Model):
	app_label = models.CharField(max_length=100)
	model = models.CharField(max_length=100)

	class Meta:
		managed = False
		db_table = 'django_content_type'
		unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
	app = models.CharField(max_length=255)
	name = models.CharField(max_length=255)
	applied = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'django_migrations'


class DjangoSession(models.Model):
	session_key = models.CharField(primary_key=True, max_length=40)
	session_data = models.TextField()
	expire_date = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'django_session'


class Film(models.Model):
	film_id = models.SmallIntegerField(primary_key=True)
	title = models.CharField(max_length=255)
	description = models.TextField(blank=True, null=True)
	release_year = models.TextField(blank=True, null=True)  # This field type is a guess.
	#language = models.ForeignKey('Language', models.DO_NOTHING)
	#original_language = models.ForeignKey('Language', models.DO_NOTHING, blank=True, null=True)
	rental_duration = models.IntegerField()
	rental_rate = models.DecimalField(max_digits=4, decimal_places=2)
	length = models.SmallIntegerField(blank=True, null=True)
	replacement_cost = models.DecimalField(max_digits=5, decimal_places=2)
	rating = models.CharField(max_length=5, blank=True, null=True)
	special_features = models.CharField(max_length=54, blank=True, null=True)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'film'


class FilmActor(models.Model):
	actor = models.ForeignKey(Actor, models.DO_NOTHING, primary_key=True)
	film = models.ForeignKey(Film, models.DO_NOTHING, primary_key=True)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'film_actor'
		unique_together = (('actor', 'film'),)


class FilmCategory(models.Model):
	film = models.ForeignKey(Film, models.DO_NOTHING)
	category = models.ForeignKey(Category, models.DO_NOTHING)
	last_update = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'film_category'
		unique_together = (('film', 'category'),)


class FilmText(models.Model):
	film_id = models.SmallIntegerField(primary_key=True)
	title = models.CharField(max_length=255)
	description = models.TextField(blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'film_text'
