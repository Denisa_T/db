from django import forms
from .models import Actor
from string import letters



CHOICES = [('search', 'Click')] + [(letter.lower(), letter) for letter in letters]
class Actors(forms.Form):
	search = forms.ChoiceField(choices = CHOICES)
