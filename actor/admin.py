from django.contrib import admin
from .models import Actor
from .models import Address
from .models import Country
from .models import City
from .models import Film
from .models import Category



admin.site.register(Actor)
admin.site.register(Address)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(Film)
admin.site.register(Category)