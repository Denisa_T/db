from django.conf.urls import url

from . import views


app_name = 'actor'
urlpatterns = [
	url(r'^index/$',
			views.IndexView.as_view(),
			name='index'),
	
	url(r'^list/$', 
			views.ActorListView.as_view(), 
			name='list'),

	url(r'^film-listing/$',
		views.ListingActors.as_view(), 
		name='listing_actors')
]