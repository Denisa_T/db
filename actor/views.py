# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.views.generic import FormView, TemplateView, ListView

from .models import Actor, Film, FilmActor
from .forms import Actors as ActorsForm


class IndexView(FormView):

	template_name = 'actor/index.html'
	form_class = ActorsForm


class ActorListView(FormView):

	template_name = 'actor/list.html'
	form_class = ActorsForm
	
	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)
			
	def get_form_kwargs(self):
		kwargs = super(ActorListView,self).get_form_kwargs() 
		kwargs['data'] = self.request.GET
		return kwargs
		
	def form_valid(self, form):
		letter = form.cleaned_data['search']
		actors = Actor.objects.filter(first_name__istartswith=letter)
		return self.render_to_response(self.get_context_data(actors=actors))


class ListingActors(ListView):
	model = FilmActor
	template_name = 'actor/list.html'
	context_object_name = 'movies'

	def get_queryset(self):
		qs = super(ListingActors, self).get_queryset()
		qs = qs.filter(actor__pk=self.request.GET.get('actor'))
		return qs

