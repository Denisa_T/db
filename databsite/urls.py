from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^actor/', include('actor.urls'), name='actor'),
    url(r'^admin/', admin.site.urls),
]